{- |
Description :  Nearly-worthless poorly-written bot made as a Haskell learning project
Copyright   :  (c) Princess Riikka
License     :  http://opensource.org/licenses/BSD-3-Clause

Maintainer  :  PrincessRiikka@gmail.com
-}
module IRCProto (User(..), Message(..), stripString, parseMessage) where
import Text.Parsec
import Text.Parsec.String (Parser)
import Data.Text (strip, pack, unpack)
import Control.Applicative ((<$>), (<*>), (<*), (*>), (<$), pure)
import Control.Monad (void)

data User = User { tag :: Char, name :: String } deriving Show

data Message = PrivMsg User String String --sourceUser sourceChannel message
  | EmoteMsg User String String --sourceUser sourceChannel message
  | JoinMsg User String
  | PartMsg User String String --sourceUser sourceChannel partMessage --Should use Maybe?
  | QuitMsg User String String --sourceUser sourceChannel partMessage
  | ModeChange User String String User --sourceUser channel newMode destUser
  | ErrorMsg String
  | Ping String

instance Show Message where
  show (PrivMsg (User tag uName) chan msg) = ('[' : chan ++ "] ") ++ ('<' : tag : uName) ++ ("> " ++ msg)
  show (EmoteMsg (User tag uName) chan msg) = ('[' : chan ++ "] ") ++ ('*' : ' ' : uName) ++ (' ' : msg)
  show (JoinMsg (User tag uName) chan) = ('[' : chan ++ "] ") ++ uName ++ " joined"
  show (PartMsg (User tag uName) chan msg) = ('[' : chan ++ "] ") ++ uName ++ " left " ++ "(" ++ stripString msg ++ ")"
  show (QuitMsg (User tag uName) chan msg) = ('[' : chan ++ "] ") ++ uName ++ " quit (" ++ stripString msg ++ ")"
  show (ModeChange (User sTag sName) chan mode (User tag uName)) = ('[' : chan ++ "] ") ++ "Mode " ++ mode ++ " " ++ uName ++ " by " ++ sName
  show (ErrorMsg msg) = "<ERR> " ++ msg
  show (Ping str) = "Ping (" ++ stripString str ++ ")"

--Remove leading/trailing whitespace
stripString :: String -> String
stripString str = unpack $ strip $ pack str

sourceUserName :: Parser User
sourceUserName = User ' ' <$> between (char ':') (char '!') (many (alphaNum <|> char '_'))

-- :: Parser ()
checkIfPM = void (manyTill anyChar (try (string "PRIVMSG")))
checkIfEmote = void (string (':':'\o001':"ACTION"))
checkIfJoin = void (manyTill anyChar (try (string "JOIN")))
checkIfPart = void (manyTill anyChar (try (string "PART")))
checkIfQuit = void (manyTill anyChar (try (string "QUIT")))
checkIfModeChange = void (manyTill anyChar (try (char ' '))) <* string "MODE"

--Or "nextWord?" <.<
sourceChannelName :: Parser String
sourceChannelName = char ' ' *> (try (manyTill anyChar (try (char ' ') <|> char '\r'))
  <|> manyTill anyChar (try eof))

nextWord = sourceChannelName --Might as well :-\
remainderToEOL = nextWord

userAtEnd :: Parser User
userAtEnd = User ' ' <$> nextWord

getMessage :: Parser String
getMessage = char ':' *> manyTill anyChar (try eof)

parseModeChange :: Parser String
parseModeChange = do
  --char ' '
  pm <- char '+' <|> char '-'
  mode <- letter
  return [pm,mode]

--Any Applicative way to get rid of the duplication?
sourcedParser = do
  source <- sourceUserName
  EmoteMsg source <$> try (checkIfPM *> sourceChannelName <* try checkIfEmote) <*> remainderToEOL
    <|> PrivMsg source <$> try (checkIfPM *> sourceChannelName) <*> getMessage
    <|> JoinMsg source <$> try (checkIfJoin *> sourceChannelName)
    <|> PartMsg source <$> try (checkIfPart *> sourceChannelName) <*> getMessage
    <|> QuitMsg source <$> try (checkIfQuit *> sourceChannelName) <*> getMessage
    <|> ModeChange source <$> try (checkIfModeChange *> sourceChannelName) <*> parseModeChange <*> userAtEnd

--This looks pretty awful... is there an Applicative style way to do this?
msgParser :: Parser Message
msgParser = try sourcedParser
          <|> Ping <$ string "PING " <*> getMessage

parseMessage str = case parse msgParser "Message Parser" str of
  Left err -> ErrorMsg (show err)
  Right msg -> msg
